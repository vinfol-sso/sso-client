package com.vinfol.filter;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSON;
import com.vinfol.pojo.DingTalkUserInfo;
import com.vinfol.pojo.ResponseResult;
import com.vinfol.pojo.User;
import com.vinfol.service.SSOAuthService;

import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SSOClientFilter implements Filter {

	@Value("/api/sso/success")
	String successUrl;

	@Value("${sso.loginurl:http://sso.vinfol.com/sso/login}")
	String ssoLoginUrl;

	@Value("${sso.loginurl:http://sso.vinfol.com/sso/dingtalk/auth/code}")
	String ssoDingTalkAuthCodeUrl;

	@Value("${sso.authentication}")
	String ssoAuthentication;

	@Value("${sso.validateTicketUrl}")
	String ssoValidateTicket;

	@Autowired
	SSOAuthService ssoAuthService;

	// 存储 Token 的 Headers Key与 Value，默认是 Authorization
	public final static String AUTHHEADERKEY = "Authorization";

	// Token 开头部分 默认 Bearer 开头
	public final static String BEARER = "Bearer ";

	public static ConcurrentHashMap<String, HttpSession> TICKET_SESSION_CACHE = new ConcurrentHashMap<String, HttpSession>();

	private final static List<String> whiteList = Arrays.asList("/api/user/login", "/rebot/message");

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;

		String jSessionId = request.getSession().getId();
		log.info("production-workbench jSessionId:{}", jSessionId);

		String path = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
				+ successUrl;
		String uri = request.getRequestURI();
		String currentUrl = request.getRequestURL().toString();
		String queryString = request.getQueryString();
		if (StringUtils.isEmpty(queryString)) {
			queryString = "";
		} else {
			queryString = "?" + queryString;
		}
		log.info("request.getRequestURL():{}", request.getRequestURL());
		String requestLogoutTicket = request.getParameter("requestLogout");
		/**
		 * 略过普通文件
		 */
		if (uri.endsWith(".css") || uri.endsWith(".js")) {
			filterChain.doFilter(servletRequest, servletResponse);
			return;
		}
		/**
		 * 重新设置cookie的方法
		 */
		if (currentUrl.endsWith("/api/sso/success") && StringUtils.hasLength(request.getParameter("token"))
				&& StringUtils.hasLength(request.getParameter("redirectUrl"))) {
			Cookie cookie = new Cookie(AUTHHEADERKEY, request.getParameter("token"));
			cookie.setMaxAge(-1);
			cookie.setPath("/");
			cookie.setDomain(request.getServerName());
			response.addCookie(cookie);
			response.sendRedirect(request.getParameter("redirectUrl"));
			return;
		}

		log.info("SSOClientFilter doFilter，url:{},path:{},uri:{},successUrl:{} ", request.getRequestURL(), path, uri,
				successUrl);

		/**
		 * 校验token
		 */
		if (!whiteList.contains(request.getRequestURI())) {

			String authorizationValue = null;
			try {
				authorizationValue = request.getHeader(AUTHHEADERKEY);
				/**
				 * header中token为空，从cookie中获取
				 */
				if (!StringUtils.hasLength(authorizationValue) || authorizationValue.indexOf(".") == -1) {
					for (Cookie cookie : request.getCookies()) {
						log.info("key:{},value:{}", cookie.getName(), cookie.getValue());
						if (AUTHHEADERKEY.equalsIgnoreCase(cookie.getName())
								&& !"undefined".equalsIgnoreCase(cookie.getValue())) {
							authorizationValue = cookie.getValue();
						}
					}
				}
			} catch (Exception e) {
				authorizationValue = null;
			}

			String code = request.getParameter("code");
			log.info("token:{},code:{}", authorizationValue, code);
			/**
			 * cookie中无登录信息，且非钉钉code授权登录
			 */
			if (StringUtils.isEmpty(authorizationValue)) {
				boolean infoError = true;
				// 且钉钉code授权登录
				if (StringUtils.hasLength(code)) {
					DingTalkUserInfo dingTalkUserInfo = ssoAuthService.getUserInfoByCode(code);
					if (dingTalkUserInfo.getErrcode() == 0 && StringUtils.hasLength(dingTalkUserInfo.getToken())) {

						Cookie cookie = new Cookie(AUTHHEADERKEY, request.getParameter("token"));
						cookie.setMaxAge(-1);
						cookie.setPath("/");
						cookie.setDomain(request.getServerName());
						response.addCookie(cookie);
						response.sendRedirect(request.getParameter("redirectUrl"));
						infoError = false;
						return;
					}
				}

				if (infoError) {
					String redirectUrl = ssoLoginUrl + "?service=" + path + "&redirectUrl=" + currentUrl + queryString;
					log.info("ssoLoginUrl:{},redirectUrl：{}", ssoLoginUrl, redirectUrl);
					response.sendRedirect(redirectUrl);
					return;
				}
			} else {

			}
		}

		String ssoTicket = request.getParameter("sso-ticket");
		if (!StringUtils.isEmpty(ssoTicket)) {
			User user = getUserByTicket(ssoTicket);
			if (user == null) {
				response.sendRedirect(ssoLoginUrl + "ERRORMSG=INVALID ACCESS");
				return;
			} else {
				response.sendRedirect(path + successUrl);
				return;
			}
		}

		if (uri.endsWith("/login")) {
			log.info("222222222222222");
			response.sendRedirect(ssoLoginUrl + "?service=" + path);
			return;
		}

		filterChain.doFilter(servletRequest, servletResponse);
	}

	private void refreshUserInfoByDingTalkAuthCode(String code) {

	}

	private void ssoLogout(String ticket) {
		if (TICKET_SESSION_CACHE.containsKey(ticket)) {
			HttpSession sess = TICKET_SESSION_CACHE.get(ticket);
			if (null != sess) {
				// SESSION_TICKET_CACHE.remove(sess.getId());
				sess.invalidate();
			}
			TICKET_SESSION_CACHE.remove(ticket);
		}

	}

	private User getUserByTicket(String ticket) {

		User user = null;
		// 传入tick获取用户信息
		CloseableHttpClient httpClient = HttpClients.createDefault();
		URI uriHttp = null;
		try {
			uriHttp = new URI(ssoValidateTicket);
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		}
		URIBuilder uriBuilder = new URIBuilder(uriHttp);
		uriBuilder.setParameter("ticket", ticket);
		URI uriParma = null;
		try {
			uriParma = uriBuilder.build();
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		}
		HttpGet httpGet = new HttpGet(uriParma);
		CloseableHttpResponse closeableHttpResponse = null;
		try {
			// 执行请求访问
			closeableHttpResponse = httpClient.execute(httpGet);
			// 获取返回HTTP状态码
			int satausCode = closeableHttpResponse.getStatusLine().getStatusCode();
			if (satausCode == 200) {
				HttpEntity entity = closeableHttpResponse.getEntity();
				String content = EntityUtils.toString(entity, "UTF-8");
				user = JSONUtil.toBean(content, User.class);
				EntityUtils.consume(entity);
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				closeableHttpResponse.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return user;
	}

	private static void printJson(HttpServletResponse response, Exception e) {
		printContent(response, JSON.toJSONString(ResponseResult.buildFailResponse(e.getMessage())));
	}

	private static void printJson(HttpServletResponse response, String msg) {
		printContent(response, JSON.toJSONString(ResponseResult.buildFailResponse(msg)));
	}

	private static void printContent(HttpServletResponse response, String content) {
		try {
			response.reset();
			response.setContentType("application/json");
			response.setHeader("Cache-Control", "no-store");
			response.setCharacterEncoding("UTF-8");
			PrintWriter pw = response.getWriter();
			pw.write(content);
			pw.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
