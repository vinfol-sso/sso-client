package com.vinfol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductionWorkbenchApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductionWorkbenchApplication.class, args);
	}

}
