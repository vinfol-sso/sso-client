package com.vinfol.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {
	@Value("${http.maxTotal:100}")
	private Integer maxTotal;

	@Value("${http.defaultMaxPerRoute:20}")
	private Integer defaultMaxPerRoute;

	@Value("${http.connectTimeout:1500}")
	private Integer connectTimeout;

	@Value("${http.connectionRequestTimeout:500}")
	private Integer connectionRequestTimeout;

	@Value("${http.socketTimeout:10000}")
	private Integer socketTimeout;

	@Value("${http.staleConnectionCheckEnabled:true}")
	private boolean staleConnectionCheckEnabled;

	@Value("${http.validateAfterInactivity:3000000}")
	private Integer validateAfterInactivity;

	@Bean
	public RestTemplate restTemplate() {
//		return new RestTemplate(httpRequestFactory());

//		RestTemplate restTemplate = new RestTemplate(httpRequestFactory());
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new WxMappingJackson2HttpMessageConverter());
		return restTemplate;
	}

	class WxMappingJackson2HttpMessageConverter extends MappingJackson2HttpMessageConverter {
		public WxMappingJackson2HttpMessageConverter() {
			List<MediaType> mediaTypes = new ArrayList<>();
			mediaTypes.add(MediaType.TEXT_PLAIN);
			mediaTypes.add(MediaType.TEXT_HTML); // 加入text/html类型的支持
			setSupportedMediaTypes(mediaTypes);// tag6
		}
	}

//	@Bean
//	public ClientHttpRequestFactory httpRequestFactory() {
//		return new HttpComponentsClientHttpRequestFactory(httpClient());
//	}
//
//	@Bean
//	public HttpClient httpClient() {
//		Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
//				.register("http", PlainConnectionSocketFactory.getSocketFactory())
//				.register("https", SSLConnectionSocketFactory.getSocketFactory()).build();
//		PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(registry);
//		connectionManager.setMaxTotal(maxTotal); // 最大连接数
//		connectionManager.setDefaultMaxPerRoute(defaultMaxPerRoute); // 单个路由最大连接数
//		connectionManager.setValidateAfterInactivity(validateAfterInactivity); // 最大空间时间
//		RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(socketTimeout) // 服务器返回数据(response)的时间，超过抛出read
//																								// timeout
//				.setConnectTimeout(connectTimeout) // 连接上服务器(握手成功)的时间，超出抛出connect timeout
//				.setStaleConnectionCheckEnabled(staleConnectionCheckEnabled) // 提交前检测是否可用
//				.setConnectionRequestTimeout(connectionRequestTimeout)// 从连接池中获取连接的超时时间，超时间未拿到可用连接，会抛出org.apache.http.conn.ConnectionPoolTimeoutException:
//																		// Timeout waiting for connection from pool
//				.build();
//		return HttpClientBuilder.create().setDefaultRequestConfig(requestConfig).setConnectionManager(connectionManager)
//				.build();
//	}

}