package com.vinfol.config;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.vinfol.filter.SSOClientFilter;

@Configuration
public class FilterConfig {
	
	@Autowired
	SSOClientFilter sSOClientFilter; 
	@Bean
	public FilterRegistrationBean<Filter> filterRegistration(){
		FilterRegistrationBean registration = new FilterRegistrationBean();
//		registration.setFilter(new SSOClientFilter());
		registration.setFilter(sSOClientFilter);
		List<String> urlList = new ArrayList<String>();
	    urlList.add("/*");
	    registration.setUrlPatterns(urlList);
		registration.setName("SSOClientFilter");
		registration.setOrder(1);
		return registration;
	}
}
