package com.vinfol.controller;
/*
* @Author: vinfol
* @Date: 2021年3月30日
*/

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	@RequestMapping("/test/hello")
	public String hello(String name) {
		return "test hello:" + name;
	}

}
