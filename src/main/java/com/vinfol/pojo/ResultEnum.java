package com.vinfol.pojo;

import lombok.Getter;

/**
 * @author Goddy
 * @date Create in 上午11:30 2018/6/25
 * @description
 */
@Getter
public enum ResultEnum {

	DUPLICATED_DATE(001, "存在重复数据，请联系管理员！"),

	OK(200, "OK"),

	SUCCESS(200, "返回成功"),

	OUT_OF_LIMIT(201, "超出使用数量限制"),

	UNKNOWN_FAILED(500, "服务器未知错误"),

	MISS_REQUEST_PARAM_ERROR(101, "缺少请求参数"),

	USERNAME_EXIST_ERROR(102, "用户已存在！"),

	CLIENT_EXIST_ERROR(103, "客户端已存在！"),

	PUBLIC_KEY_NOT_EXIST(104, "公钥不存在！"),

	CLIENT_NOT_EXIST_ERROR(106, "客户端不存在！"),

	USERNAME_NOT_EXIST_ERROR(107, "用户不存在！"),

	OUT_OF_LIMIT_TIME(108, "超出限制时间"),

	VERIFY_SIGNATURE_ERROR(109, "校验签名失败！"),

	ROLE_NOT_EXIST_ERROR(110, "角色不存在！"),

	CANT_NOT_EDIT_MYSELF_ROLE(111, "不能修改自己的角色"),

	AUTH_FAILED(112, "用户名密码校验失败"),

	RESOURCE_DENEY(403, "资源无权限访问或被禁止访问"),

	RESOURCE_NOT_FOUND(404, "资源未找到"),

	USER_NOT_FOUND(404, "账号未找到"),

	USER_HAS_EXISTS(501, "账号已存在"), INVALID_PARAM(100000, "参数错误"),

	SAVE_FAILED(888888, "保存失败"),

	UPDATE_FAILED(777777, "保存失败"),

	DELTE_FAILED(666666, "删除失败"), SEARCH_FLOW_FAILED(555555, "查询任务流的执行详情失败!"),

	TOKEN_INVALID(50008, "Token 缺失或已失效"),

	CONTINUE(100, "Continue"),

	SWITCHING_PROTOCOLS(101, "Switching Protocols"),

	PROCESSING(102, "Processing"),

	CHECKPOINT(103, "Checkpoint"),

	CREATED(201, "Created"),

	ACCEPTED(202, "Accepted"), NON_AUTHORITATIVE_INFORMATION(203, "Non-Authoritative Information"),
	NO_CONTENT(204, "No Content"), RESET_CONTENT(205, "Reset Content"), PARTIAL_CONTENT(206, "Partial Content"),
	MULTI_STATUS(207, "Multi-Status"), ALREADY_REPORTED(208, "Already Reported"), IM_USED(226, "IM Used"),

	MULTIPLE_CHOICES(300, "Multiple Choices"), MOVED_PERMANENTLY(301, "Moved Permanently"), FOUND(302, "Found"),
	SEE_OTHER(303, "See Other"), NOT_MODIFIED(304, "Not Modified"), TEMPORARY_REDIRECT(307, "Temporary Redirect"),
	PERMANENT_REDIRECT(308, "Permanent Redirect"),

	BAD_REQUEST(400, "Bad Request"), UNAUTHORIZED(401, "Unauthorized"), PAYMENT_REQUIRED(402, "Payment Required"),
	FORBIDDEN(403, "Forbidden"), NOT_FOUND(404, "Not Found"), METHOD_NOT_ALLOWED(405, "Method Not Allowed"),
	NOT_ACCEPTABLE(406, "Not Acceptable"), PROXY_AUTHENTICATION_REQUIRED(407, "Proxy Authentication Required"),
	REQUEST_TIMEOUT(408, "Request Timeout"), CONFLICT(409, "Conflict"), GONE(410, "Gone"),
	LENGTH_REQUIRED(411, "Length Required"), PRECONDITION_FAILED(412, "Precondition Failed"),
	PAYLOAD_TOO_LARGE(413, "Payload Too Large"), URI_TOO_LONG(414, "URI Too Long"),
	UNSUPPORTED_MEDIA_TYPE(415, "Unsupported Media Type"),
	REQUESTED_RANGE_NOT_SATISFIABLE(416, "Requested range not satisfiable"),
	EXPECTATION_FAILED(417, "Expectation Failed"), I_AM_A_TEAPOT(418, "I'm a teapot"),
	UNPROCESSABLE_ENTITY(422, "Unprocessable Entity"), LOCKED(423, "Locked"),
	FAILED_DEPENDENCY(424, "Failed Dependency"), UPGRADE_REQUIRED(426, "Upgrade Required"),
	PRECONDITION_REQUIRED(428, "Precondition Required"), TOO_MANY_REQUESTS(429, "Too Many Requests"),
	REQUEST_HEADER_FIELDS_TOO_LARGE(431, "Request Header Fields Too Large"),
	UNAVAILABLE_FOR_LEGAL_REASONS(451, "Unavailable For Legal Reasons"),

	INTERNAL_SERVER_ERROR(500, "Internal Server Error"), NOT_IMPLEMENTED(501, "Not Implemented"),
	BAD_GATEWAY(502, "Bad Gateway"), SERVICE_UNAVAILABLE(503, "Service Unavailable"),
	GATEWAY_TIMEOUT(504, "Gateway Timeout"), HTTP_VERSION_NOT_SUPPORTED(505, "HTTP Version not supported"),
	VARIANT_ALSO_NEGOTIATES(506, "Variant Also Negotiates"), INSUFFICIENT_STORAGE(507, "Insufficient Storage"),
	LOOP_DETECTED(508, "Loop Detected"), BANDWIDTH_LIMIT_EXCEEDED(509, "Bandwidth Limit Exceeded"),
	NOT_EXTENDED(510, "Not Extended"), NETWORK_AUTHENTICATION_REQUIRED(511, "Network Authentication Required");

	private Integer code;

	private String message;

	ResultEnum(Integer code, String message) {
		this.code = code;
		this.message = message;
	}
}
