package com.vinfol.pojo;

import lombok.Data;

/**
 * @Author: ivan
 * @Description: 返回值封装
 * @Date: Created in 17:26 18/11/26
 * @Modified By:
 */
@Data
public class ResponseResult<T> {

//	public static final ObjectMapper mapper = new ObjectMapper();

	private int status;

	private int code;

	private String message;

	private String msg;

	private Object data;

	public ResponseResult(Object data) {
		this.data = data;
	}

	public ResponseResult(ResponseStatus status, ResultEnum resultEnum, String message, T data) {
		this.setStatus(status);
		this.setCode(resultEnum);
		this.setMessage(message);
		this.setMsg(message);
		this.setData(data);
	}

	public boolean isSuccess() {
		return status == ResponseStatus.OK.getValue();
	}

	public static <T> ResponseResult<T> buildSuccessResponse(T data) {
		return new ResponseResult<T>(ResponseStatus.OK, ResultEnum.SUCCESS, null, data);
	}

	public static <T> ResponseResult<T> buildFailResponse(String message) {
		return new ResponseResult<T>(ResponseStatus.ERROR, ResultEnum.UNKNOWN_FAILED, message, null);
	}

	public static <T> ResponseResult<T> buildFailResponse(ResultEnum resultEnum) {
		return new ResponseResult<T>(ResponseStatus.ERROR, ResultEnum.UNKNOWN_FAILED, resultEnum.getMessage(), null);
	}

	public static <T> ResponseResult<T> buildFailResponse(ResultEnum resultEnum, String message) {
		return new ResponseResult<T>(ResponseStatus.ERROR, ResultEnum.UNKNOWN_FAILED, message, null);
	}

	public static <T> ResponseResult<T> buildFailResponse(ResponseStatus responseStatus, ResultEnum resultEnum,
			String message, T data) {
		return new ResponseResult<T>(responseStatus, resultEnum, message, data);
	}

	public static ResponseResult<String> buildFailResponse(ResponseStatus responseStatus, ResultEnum resultEnum) {
		return new ResponseResult<String>(responseStatus, resultEnum, responseStatus.getMessage(), null);
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(ResponseStatus status) {
		this.status = status.getValue();
	}

	public int getCode() {
		return code;
	}

	public void setCode(ResultEnum resultEnum) {
		this.code = resultEnum.getCode();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

//	@Override
//	public String toString() {
//		try {
//			return mapper.writeValueAsString(this);
//		} catch (JsonProcessingException e) {
//			e.printStackTrace();
//			return e.getMessage();
//		}
//	}
}