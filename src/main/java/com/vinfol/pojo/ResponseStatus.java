package com.vinfol.pojo;

/**
 * @Author: ivan
 * @Description: 服务状态代码
 * @Date: 18/11/26
 * @Modified By;
 */
public enum ResponseStatus {

	OK(0, "成功"), ERROR(-1, "服务错误");

	private int value;
	private String message;

	ResponseStatus(int value, String message) {
		this.value = value;
		this.message = message;
	}

	public int getValue() {
		return value;
	}

	public String getMessage() {
		return message;
	}

}