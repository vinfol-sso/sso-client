package com.vinfol.pojo;

import lombok.Data;

/*
* @Author: vinfol
* @Date: 2021年3月30日
*/
@Data
public class DingTalkUserInfo {
	int errcode;
	String errmsg;
	String userid;
	String name;
	String deviceId;
	boolean is_sys;
	int sys_level;
	String token;
	long token_expired;
}
