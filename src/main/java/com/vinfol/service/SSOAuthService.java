package com.vinfol.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.vinfol.pojo.DingTalkUserInfo;

import lombok.extern.slf4j.Slf4j;

/*
* @Author: vinfol
* @Date: 2021年3月30日
*/
@Service
@Slf4j
public class SSOAuthService {

	@Autowired
	RestTemplate restTemplate;

	@Value("${sso.getuserinfobycode:http://sso.vinfol.com/sso/dingtalk/auth/code}")
	String ssoGetUserInfoBycode;

	public DingTalkUserInfo getUserInfoByCode(String code) {
		String requestUrl = ssoGetUserInfoBycode + "?code=" + code;
		log.info("getDingTalkAccessTokenByAppKey, requestUrl:{}", requestUrl);
		DingTalkUserInfo dingTalkUserInfo = restTemplate.getForObject(requestUrl, DingTalkUserInfo.class);
		return dingTalkUserInfo;

	}
}
